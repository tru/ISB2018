# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# module fix for tcl in LD_LIBRAY_PATH
module() {
 if [ `uname -m` == "x86_64" ]; then
    eval `/lib64/ld-linux-x86-64.so.2 --library-path '' /usr/bin/modulecmd bash $*`
 else
    eval `/lib/ld-linux.so.2 --library-path '' /usr/bin/modulecmd bash $*`
 fi
}
# User specific aliases and functions
module purge
module use /ISB2018/modulefiles

# July17 setup
module add pymol/1.8.6.0
source /ISB2018/ccp4-7.0/bin/ccp4.setup-sh
module add imosflm/7.2.2-linux-64
source /ISB2018/phenix-1.13-2998/phenix_env.sh
module add ATSAS
module add xds/2018-07-04 xdsme/0.6.2
module add opt-pi-bin
