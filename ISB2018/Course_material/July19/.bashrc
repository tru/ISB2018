# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=

# module fix for tcl in LD_LIBRAY_PATH
module() {
 if [ `uname -m` == "x86_64" ]; then
    eval `/lib64/ld-linux-x86-64.so.2 --library-path '' /usr/bin/modulecmd bash $*`
 else
    eval `/lib/ld-linux.so.2 --library-path '' /usr/bin/modulecmd bash $*`
 fi
}
# User specific aliases and functions
module purge
module use /ISB2018/modulefiles

# July19 setup
#module add chimera
alias chimera=/ISB2018/chimera/1.12-linux_x86_64/bin/chimera
echo "module add anaconda2-sphire"
echo "or"
echo "module add anaconda2-cryolo"
source /ISB2018/ccp4-7.0/bin/ccp4.setup-sh
module add imosflm/7.2.2-linux-64
source /ISB2018/phenix-1.13-2998/phenix_env.sh

