mkdir -p /home/isb2018/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC4q49GrsVzlBQd+qfFN9otEHrvEfJapvEPx713FI70iGFDJ5d2n7RRERtju8r/u3XwmrctmarZr6ZKamPq6QdDplLhm+/W1jOvsm6qQuPHpyze/QTnnbCgkgg+d8okHBg/9FM2XSk/0mqmBtfxb78MeDJ3B6Y2iEhVtO9+BuEe9i0o0yhGlPgOt/HYpVIZ7z2ePZijk+IzL4/TD5JyhdQ9iEDzlvNXbzXDAKppUlE6GQq1wR8pm++IsKMJ/tGs38CPwc4F8vZkYac1izsvZPSheDNRstXb9tWYGsUdj2TVOba8wZQ7nnQCVFWDdWT8Z98JLsW1U1gwYSFf46J/E8yX isb2018' > /home/isb2018/.ssh/authorized_keys
chmod 0700 /home/isb2018/.ssh
chmod 0600 /home/isb2018/.ssh/authorized_keys
chown -R isb2018:isb2018 /home/isb2018/.ssh
restorecon -rv /home/isb2018
