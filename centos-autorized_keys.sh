mkdir -p /home/centos/.ssh
echo 'ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDNRa16a8mbCipyoPnNe3NhjfeFNl65iWbe0RqzDyFZUrn5vnmgn5DSzOtDxsk7aRaF4FlqRl64pzcv+2qgijoCuAIJl0Z4XYQ1ofKpwN0nM/sNAPm3u8OmPlDVxt6/UBtD17G83NuTB5hevF4FI78oHgbNQWpI6F7/RvWcREKpYXbmDjZKVQwJg8ptnJ4wWUDLA3T+ELJPeTMiVgH7ZjY9kbpCLDTMC7sseYMuotGa3JiI/qb0yHh+zGOus9ENRijOOdxI6vYyZ37jTHRRuVWFVUa9ZHUe57kZwiOPKs35DbTLlBu2R1/BDa0vRPTnC1xlzHPt4AC9jo109DRB5gcL centos' > /home/centos/.ssh/authorized_keys
chmod 0700 /home/centos/.ssh
chmod 0600 /home/centos/.ssh/authorized_keys
chown -R centos:centos /home/centos/.ssh
restorecon -rv /home/centos
echo 'centos ALL=(ALL) NOPASSWD: ALL' >> /etc/sudoers
restorecon -rv /etc/sudoers
